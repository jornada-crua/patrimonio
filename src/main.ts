import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from "./store/store"
import VueMapboxTs from "vue-mapbox-ts"
import VueGtag from "vue-gtag"
import 'vue-plyr/dist/vue-plyr.css'

const app = createApp(App)
app.use(router)
app.use(store)
app.use(VueMapboxTs)
app.use(VueGtag, {
  config: { id: "G-D5V828T5N9" }
})
app.mount('#app')
