import { createWebHistory, createRouter } from "vue-router"
import Main from "../views/Main.vue"
import Home from "../views/Home.vue"
import Guide from "../views/Guide.vue"
import Mapa from "../views/Map.vue"
import Tests from "../views/Tests.vue"
import Circuitos from "../views/Circuitos.vue"
import Retratos from "../views/Retratos.vue"
import Galerias from "../views/Galerias.vue"
import GaleriaTour from "../views/GaleriaTour.vue"
import Audioguias from "../views/Audioguias.vue"
import AudioTour from "../views/AudioTour.vue"
import Placas from "../views/Placas.vue"
import Sobre from "../views/Sobre.vue"

const routes = [
  {
    path: "/",
    name: "main",
    component: Main
  },
  {
    path: "/home",
    name: "home",
    component: Home
  },
  {
    path: "/circuitos/",
    redirect: "/circuitos/samba"
  },
  {
    path: "/circuitos/:tour",
    name: "circuitos",
    component: Circuitos
  },
  {
    path: "/retratos",
    name: "retratos",
    component: Retratos
  },
  {
    path: "/galerias",
    name: "galerias",
    component: Galerias
  },
  {
    path: "/galerias/:tour",
    name: "GaleriaTour",
    component: GaleriaTour
  },
  {
    path: "/audioguias",
    name: "audioguias",
    component: Audioguias
  },
  {
    path: "/audioguias/:tour",
    name: "AudioTour",
    component: AudioTour
  },
  {
    path: "/placas/:tour?",
    name: "placas",
    component: Placas
  },
  {
    path: "/guia/:tour",
    name: "Guide",
    component: Guide
  },
  {
    path: "/mapa",
    name: "Map",
    component: Mapa
  },
  {
    path: "/Sobre",
    name: "Sobre",
    component: Sobre
  },
  {
    path: '/:pathMatch(.*)',
    component: Home
  }

]

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    // always scroll to top
    return { top: 0 }
  },
})

export default router
