export type State = {
  map: {
    key: string, style: string
  }
  pages: Pages
  circuits: Circuits
  retratos: Retratos
  gallerys: Gallerys
  overlayVideo: string
  openFullscreen: boolean
  muted: boolean
  accessibilityMode: boolean
  galeriasInfo: boolean
  placasInfo: boolean
  homeAnimation: true
  audioGuiasAnimation: boolean
}

export type Pages = {
  [key: string]: {
    src: string
    image: string
    name: string
    slug?: string
    description: string
    instruction: string
  }
}

export type Circuits = {
  [key: string]: {
    name: string
    number: number
    color: number
    about: string
    video: string[]
    images: string[]
    audioguiaImgs?: string[]
    audioguiaAudio?: string[]
    audioguiaText?: string
    audioguiaTimes?: number[]
    gridImg?: string[]
    circuitImg?: string[]
  }
}

export type Retratos = {
  [key: string]: {
    selector: string
    image: string
    video: string
    name: string
    subtitle: string
    circuit: string
    position: string
  }
}

export type Gallerys = {
  [key: string]: {
    src: string,
    name: string,
    image: string
  }
}