export default {
  stacasa: {
    name: 'Irmandade da Santa Casa de Misericórdia de São Paulo',
    src: 'https://www.vila360.com.br/tour/santacasa/',
    image: 'stacasa'
  },
  pinel: {
    name: 'Hospital Psiquiátrico Philippe Pinel',
    src: 'https://www.vila360.com.br/tour/hospitalphilippepinel/',
    image: 'pinel'
  },
  adolfo: {
    name: 'Instituto Adolfo Lutz',
    src: 'https://www.vila360.com.br/tour/institutoadolfolutz/',
    image: 'lutz'
  },
  vicentino: {
    name: 'Colégio Vicentino de Cegos Padre Chico',
    src: 'https://www.vila360.com.br/tour/institutopadrechico/',
    image: 'chico'
  },
  pasteur: {
    name: 'Instituto Pasteur',
    src: 'https://www.vila360.com.br/tour/institutopasteur/',
    image: 'pasteur'
  },
  dpedro: {
    name: 'Hospital Geriátrico e de Convalescentes Dom Pedro II',
    src: 'https://www.vila360.com.br/tour/hospitaldompedroii/',
    image: 'pedroII'
  },
  alvarenga: {
    name: 'Hospital Dom Alvarenga',
    src: 'https://www.vila360.com.br/tour/hospitaldomalvarenga/',
    image: 'alvarenga'
  },
  casamadre: {
    name: 'Casa Madre Assunta Marchetti',
    src: 'https://www.vila360.com.br/tour/casamadreassunta/',
    image: 'madre'
  },
  dduarte: {
    name: 'Educandário Dom Duarte',
    src: 'https://www.vila360.com.br/tour/emefdomduarte/',
    image: 'duarte'
  },
  butanta: {
    name: 'Instituto Butantan',
    src: 'https://www.vila360.com.br/tour/institutobutantan/',
    image: 'butantan'
  }
}


