export default {
  "a": {
    "selector": "personagem",
    "image": "image-0.jpg",
    "video": "video-0",
    "name": "Carmen e Sandro",
    "subtitle": "Projecionistas e cinéfilos",
    "circuit": "cinema",
    "position": "right-top",
    "audio": "audio-0.mp3"
  },
  "b": {
    "selector": "personagem",
    "image": "image-1.jpg",
    "video": "video-1",
    "name": "Aline e Alessandra",
    "subtitle": "Criadoras da Maternidade Sapatão",
    "circuit": "lgbtqia",
    "position": "bottom-left",
    "audio": "audio-1.mp3"
  },
  "c": {
    "selector": "personagem",
    "image": "image-2.jpg",
    "video": "video-2",
    "name": "Carlão do Peruche",
    "subtitle": "Fundador do G.R.C.S.E.S Unidos do Peruche",
    "circuit": "samba",
    "position": "left-top",
    "audio": "audio-2.mp3"
  },
  "d": {
    "selector": "personagem",
    "image": "image-3.jpg",
    "video": "video-3",
    "name": "Sonia Ara Mirim",
    "subtitle": "Liderança da aldeia Guarani Mbya Tekoa Ytu",
    "circuit": "indigenas",
    "position": "right-top",
    "audio": "audio-3.mp3"
  },
  "e": {
    "selector": "personagem",
    "image": "image-4.jpg",
    "video": "video-4",
    "name": "Aílton Pedro",
    "subtitle": "Proprietário do Guinga's Bar",
    "circuit": "lgbtqia",
    "position": "left-bottom",
    "audio": "audio-4.mp3"
  },
  "f": {
    "selector": "personagem",
    "image": "image-5.jpg",
    "video": "video-5",
    "name": "Álvaro Machado",
    "subtitle": "Jornalista e pesquisador teatral",
    "circuit": "teatro",
    "position": "right-top",
    "audio": "audio-5.mp3"
  },
  "g": {
    "selector": "personagem",
    "image": "image-6.jpg",
    "video": "video-6",
    "name": "Elizete Alves",
    "subtitle": "União dos Amigos da Capela dos Aflitos",
    "circuit": "resistencia",
    "position": "bottom-left",
    "audio": "audio-6.mp3"
  },
  "h": {
    "selector": "personagem",
    "image": "image-7.jpg",
    "video": "video-7",
    "name": "Anderson Félix de Sá",
    "subtitle": "Arquiteto e Pesquisador",
    "circuit": "saude",
    "position": "bottom-right",
    "audio": "audio-7.mp3"
  },
  "i": {
    "selector": "personagem",
    "image": "image-8.jpg",
    "video": "video-8",
    "name": "Jerá Guarani",
    "subtitle": "Pedagoga e liderança da aldeia Tenonde Porã",
    "circuit": "indigenas",
    "position": "bottom-left",
    "audio": "audio-8.mp3"
  },
  "j": {
    "selector": "personagem",
    "image": "image-9.jpg",
    "video": "video-9",
    "name": "Bruno Carvalho",
    "subtitle": "Arquiteto e urbanista",
    "circuit": "arquitetura",
    "position": "bottom-left",
    "audio": "audio-9.mp3"
  },
  "k": {
    "selector": "personagem",
    "image": "image-11.jpg",
    "video": "video-11",
    "name": "Carlos Warchavchik",
    "subtitle": "Arquiteto ",
    "circuit": "arquitetura",
    "position": "bottom-right",
    "audio": "audio-11.mp3"
  },
  "l": {
    "selector": "personagem",
    "image": "image-12.jpg",
    "video": "video-12",
    "name": "Paula de Yansã",
    "subtitle": "Ialorixá do Terreiro Axé Ilê Obá",
    "circuit": "resistencia",
    "position": "bottom-left",
    "audio": "audio-12.mp3"
  },
  "m": {
    "selector": "personagem",
    "image": "image-13.jpg",
    "video": "video-13",
    "name": "Deborah Neves",
    "subtitle": "Historiadora",
    "circuit": "operarios",
    "position": "right-top",
    "audio": "audio-13.mp3"
  },
  "n": {
    "selector": "personagem",
    "image": "image-14.jpg",
    "video": "video-14",
    "name": "Mari Marinho",
    "subtitle": "Atriz",
    "circuit": "teatro",
    "position": "right-top",
    "audio": "audio-14.mp3"
  },
  "o": {
    "selector": "personagem",
    "image": "image-15.jpg",
    "video": "video-15",
    "name": "Edson Sena",
    "subtitle": "Presidente da SADE",
    "circuit": "futebol",
    "position": "bottom-left",
    "audio": "audio-15.mp3"
  },
  "p": {
    "selector": "personagem",
    "image": "image-16.jpg",
    "video": "video-16",
    "name": "Louise Lenate",
    "subtitle": "Arquiteta e Urbanista",
    "circuit": "operarios",
    "position": "left-top",
    "audio": "audio-16.mp3"
  },
  "q": {
    "selector": "personagem",
    "image": "image-17.jpg",
    "video": "video-17",
    "name": "Paula Nishida",
    "subtitle": "Arqueóloga ",
    "circuit": "resistencia",
    "position": "right-top",
    "audio": "audio-17.mp3"
  },
  "r": {
    "selector": "personagem",
    "image": "image-18.jpg",
    "video": "video-18",
    "name": "Francisco Ingegnere",
    "subtitle": "Presidente do Santa Marina Atlético Clube",
    "circuit": "futebol",
    "position": "left-bottom",
    "audio": "audio-18.mp3"
  },
  "s": {
    "selector": "personagem",
    "image": "image-19.jpg",
    "video": "video-19",
    "name": "Inimá Simões",
    "subtitle": "Jornalista e escritor",
    "circuit": "cinema",
    "position": "right-top",
    "audio": "audio-19.mp3"
  },
  "t": {
    "selector": "personagem",
    "image": "image-20.jpg",
    "video": "video-20",
    "name": "Rose Moraes",
    "subtitle": "Ex-presidente da Escola de Samba Lavapés",
    "circuit": "samba",
    "position": "bottom-left",
    "audio": "audio-20.mp3"
  },
  "u": {
    "selector": "personagem",
    "image": "image-21.jpg",
    "video": "video-21",
    "name": "Abilio Ferreira",
    "subtitle": "Escritor e jornalista",
    "circuit": "resistencia",
    "position": "bottom-right",
    "audio": "audio-21.mp3"
  },
  "v": {
    "selector": "personagem",
    "image": "image-22.jpg",
    "video": "video-22",
    "name": "Maria Luíza",
    "subtitle": "Diretora da Casa de Apoio Brenda Lee ",
    "circuit": "saude",
    "position": "bottom-left",
    "audio": "audio-22.mp3"
  },
  "w": {
    "selector": "personagem",
    "image": "image-23.jpg",
    "video": "video-23",
    "name": "Rose Almeida",
    "subtitle": "Associação Cultural Vila Maria Zélia",
    "circuit": "operarios",
    "position": "bottom-left",
    "audio": "audio-23.mp3"
  },
  "x": {
    "selector": "personagem",
    "image": "image-24.jpg",
    "video": "video-24",
    "name": "José Possi",
    "subtitle": "Diretor de teatro",
    "circuit": "teatro",
    "position": "bottom-right",
    "audio": "audio-24.mp3"
  },
  "y": {
    "selector": "personagem",
    "image": "image-25.jpg",
    "video": "video-25",
    "name": "Otacílio Ribeiro",
    "subtitle": "Secretário do Complexo de Campos do Campo de Marte",
    "circuit": "futebol",
    "position": "bottom-left",
    "audio": "audio-25.mp3"
  }
}
