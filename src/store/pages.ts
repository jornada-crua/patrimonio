export default {
  circuitos: {
    name: 'circuitos',
    image: '',
    description: '',
    instruction: '',
    
  },
  retratos: {
    name: 'retratos',
    image: 'Home_2_retratos',
    slug: `
      Ouça as vozes de quem faz, estuda e reconta a história paulistana
    `,
    description: `
      A história existe na vivência das pessoas. Conheça alguns personagens que fazem parte dessa nossa jornada.
    `,
    instruction: `
      Clique, ouça os depoimentos e, se quiser, amplie o passeio fazendo todo o circuito a que esse retrato está ligado.
    `
  },
  galerias: {
    name: 'galerias 360',
    image: 'Home_2_galeria360',
    slug: `
      Faça um passeio em 360º por alguns dos locais mais representativos da saúde e assistência na cidade
    `,
    description: `
      Arraste para o lado e dê um giro por esses locais tão importantes
    `,
    instruction: `
      Selecionamos 10 lugares icônicos relacionados ao acolhimento e à assistência em São Paulo para você conhecer por meio de imagens 360º. Essa é uma maneira de se observar detalhes de algumas instituições fundamentais para a saúde da nossa cidade.
    `
  },
  audioguias: {
    name: 'audioguias',
    image: 'Home_2_audioguias',
    slug: `
      Ouça e siga os circuitos, descobrindo a cidade de um novo jeito e atualizando a história
    `,
    description: `
      Cada um dos nossos circuitos tem um audioguia. Aproveite sua viagem ouvindo sobre cada um dos pontos e, conforme o som se desenrola, acompanhe também imagens fascinantes dessas histórias. Dê o play e aproveite a jornada!
    `,
    instruction: ''
  },
  
  placas: {
    name: 'PLACAS DA MEMÓRIA',
    image: 'Home_2_placas-da-memoria',
    slug: `
      Navegue no mapa pelos principais
      pontos da memória paulistana
    `,
    description: `
      O Inventário Memória Paulistana já mapeou mais de 400 placas, lugares importantes na cidade que devem ser conhecidos e lembrados.
    `,
    instruction: `
      Muitos desses pontos fazem parte dos nossos circuitos temáticos e conectam histórias que se relacionam e completam. Navegue pelo mapa e clique para saber mais sobre cada ponto ou iniciar um circuito. 
    `
  },
  sobre: {
    name: 'sobre',
    slug: `
      Uma viagem por diferentes cidades dentro da cidade
    `,
    description: `
      <p>Esta plataforma foi criada para apresentar ao público algumas histórias especiais da cidade de São Paulo a partir de circuitos temáticos e dos seus personagens. O projeto é parte integrante da Jornada do Patrimônio 2021, um evento de salvaguarda da história paulistana que entrelaça ações da sociedade e do poder público para a preservação de memórias paulistanas.</p>
      <p>A partir de mais de 400 pontos mapeados pelo projeto Placas da Memória Paulistana, que realçam locais de interesse histórico na cidade, criamos circuitos temáticos em uma experiência virtual multimídia. Esses circuitos contam a história e apresentam sentimentos e memórias dos diferentes grupos que os representam. </p>
      <p>A partir desses caminhos, o público também pode conhecer um pouco mais as sociabilidades do cinema, a memória operária, as histórias da assistência à saúde na cidade, resistência negra, lutas da população LGBTQIA+, histórias do teatro, do samba, da arquitetura modernista, o futebol de várzea e os povos indígenas paulistanos.</p>
    `,
    instruction: ``,
  }
}
