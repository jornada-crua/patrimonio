import { createStore } from "vuex"
import { State } from "../types/types"
import circuits from "./circuits"
import gallerys from "./gallerys"
import pages from "./pages"
import retratos from "./retratos"

const state: State = {
  map: {
    key: 'pk.eyJ1IjoiYWl2dWsiLCJhIjoiY2tycWltdW85MXIxMzJ3bm95cTVwb2tsdSJ9.mCLZlIzSfuM0hBk8e_3sdw',
    style: 'mapbox://styles/aivuk/ckrqfyvwo622l17pdo0u0ihzo'
  },
  pages: pages,
  circuits: circuits,
  retratos: retratos,
  gallerys: gallerys,
  overlayVideo: '',
  openFullscreen: false,
  muted: false,
  galeriasInfo: false,
  placasInfo: false,
  homeAnimation: true,
  audioGuiasAnimation: true,
  accessibilityMode: false
}

const mutations = {
  closeVideo (state) {
    state.openFullscreen = false
  },
  openVideo (state, videoSrc: string) {
    state.openFullscreen = true
    state.overlayVideo = videoSrc
  },
  toggleMute (state) {
    state.muted = !state.muted
  },
  toggleAccessibilityMode(state) {
    state.accessibilityMode = !state.accessibilityMode;
  },
  closeGaleriasInfo (state) {
    state.galeriasInfo = true
  },
  closePlacasInfo (state) {
    state.placasInfo = true
  },
  disableHomeAnimation (state) {
    state.homeAnimation = false
  },
  disableAudioguiasAnimation (state) {
    state.audioGuiasAnimation = false
  }
}

const store = createStore({
   state,
   mutations
})

export default store