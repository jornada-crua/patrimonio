module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'map-blue': '#0833A8',
        'map-purple': '#7670B2',
        'light-blue': '#56C4CF',
        'light-gray': '#EAEAEA',
        'mid-gray': '#D3D3D3',
        'dark-gray': '#707070',
        'darker-gray': '#262727',
        'strong-yellow': '#FFCE00'
      },
      fontSize: {
        '2xs': '.6rem',
      },
      margin: {
        'screen-h': '100vh'
      },
      boxShadow: {
        'inner-top': 'inset 0 0 100px 0 rgba(0,0,0,0.5)'
      },
      screens: {
        'portrait': {'raw': '(orientation: portrait)'},
        'mobile-landscape': {'raw': 'screen and (max-device-width: 900px) and (orientation: landscape)'}
      },
      strokeWidth: {
        '3': '3',
        '4': '4',
        '5': '5',
        '6': '6',
        '7': '7',
        '8': '8',
        '9': '9',
        '10': '10',
        '11': '11',
        '12': '12',
        '13': '13',
        '14': '14'
       }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
  ],
};
